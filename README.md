# Observer Pattern

The repository contains a Java application for Observer Pattern example.

The Strategy pattern is known as a behavioural pattern - it's used to manage algorithms, relationships and responsibilities between objects.

## Built With

* [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java SE Development Kit 
* [IntelliJIDEA](https://www.jetbrains.com/idea/) - IDE for Java
* [Git](https://git-scm.com/) - The distributed version control system

## Author

* **María Ruth Vargas**